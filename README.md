# container pipeline

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/container?branch=main)](https://gitlab.com/buildgarden/pipelines/container/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/buildgarden/pipelines/container)](https://gitlab.com/buildgarden/pipelines/container/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Build [OCI](https://opencontainers.org/) container images.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Usage

### Project setup

Add a Dockerfile to the root of the project.

### Build a container with Docker

The default configuration builds for whatever platform on which the job is
running:

```yaml
include:
  - project: buildgarden/pipelines/container
    file:
      - container-docker.yml
```

You can build multiple containers in your project using
[`parallel:matrix`](https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix):

```yaml
include:
  - project: buildgarden/pipelines/container
    file:
      - container-docker.yml

container-docker:
  parallel:
    matrix:
      - CONTAINER_NAME_SUFFIX: "-fedora"
        CONTAINER_DOCKERFILE: "${CI_PROJECT_DIR}/Dockerfile.fedora"
      - CONTAINER_NAME_SUFFIX: "-ubuntu"
        CONTAINER_DOCKERFILE: "${CI_PROJECT_DIR}/Dockerfile.ubuntu"
```

#### Build a multiarch container

```yaml
include:
  - project: buildgarden/pipelines/container
    file:
      # choose the architectures you want
      - container-docker-multiarch-amd64.yml
      - container-docker-multiarch-arm32v6.yml
      - container-docker-multiarch-arm32v7.yml
      - container-docker-multiarch-arm64v8.yml
      - container-docker-multiarch-i386.yml

      # this job must always be included
      - container-docker-multiarch-manifest.yml
```

#### Build for another architecture

```yaml
include:
  - project: buildgarden/pipelines/container
    file:
      - container-docker-multiarch.yml

      # this job must always be included
      - container-docker-multiarch-manifest.yml

container-docker-multiarch-mips64:
  extends: container-docker-multiarch
  variables:
    CONTAINER_DOCKER_ARCH: mips64
    CONTAINER_DOCKER_MACHINE_ARCH: mips64
    CONTAINER_DOCKER_PLATFORM: linux/mips64
```

### Build containers with [kaniko](https://github.com/GoogleContainerTools/kaniko)

Kaniko is an alternative container build tool.

```yaml
include:
  - project: buildgarden/pipelines/container
    file:
      - container-kaniko.yml
```

### Lint Dockerfiles with [hadolint](https://github.com/hadolint/hadolint)

```yaml
include:
  - project: buildgarden/pipelines/container
    file:
      - container-hadolint.yml
```

### Customize the image tag name

The following variables are available to customize the generated image tag
names:

- `CONTAINER_IMAGE`
- `CONTAINER_NAME`
- `CONTAINER_NAME_SUFFIX`
- `CONTAINER_VERSION_PREFIX`
- `CONTAINER_VERSION`
- `CONTAINER_VERSION_SUFFIX`

### Pass build options

`CONTAINER_BUILD_OPTS` may be used to pass extra arguments to the container
build command.

Docker and Kaniko sometimes have different names for the same option.

- For Docker jobs, see <https://docs.docker.com/engine/reference/commandline/buildx_build/>.
- For Kaniko jobs, see <https://github.com/GoogleContainerTools/kaniko#additional-flags>.

#### `--build-arg`

`--build-arg` is the same for Docker and Kaniko.

```yaml
container-kaniko:
  variables:
    CONTAINER_BUILD_OPTS: >-
      --destination ${CONTAINER_NAME}:3.4.4-${CONTAINER_VERSION}
      --destination ${CONTAINER_NAME}:3.4-${CONTAINER_VERSION}
      --destination ${CONTAINER_NAME}:3-${CONTAINER_VERSION}
```

```yaml
container-kaniko:
  variables:
    CONTAINER_BUILD_OPTS: >-
      --build-arg PIP_INDEX_URL=$PYTHON_PYPI_DOWNLOAD_URL
      --platform linux/arm/v6
```

### Use a container proxy

Define `CONTAINER_PROXY` to store the container proxy URL; in this case,
GitLab's [Dependency
Proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/):

```yaml
variables:
  CONTAINER_PROXY: "${CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX}/"
```

> **NOTE:** the trailing slash (`/`) allows projects to function when
> `CONTAINER_PROXY` is not configured.

Use `CONTAINER_PROXY` from a CI job:

```yaml
image: "${CONTAINER_PROXY}alpine:latest"
```

Use `CONTAINER_PROXY` from a Dockerfile:

```yaml
ARG CONTAINER_PROXY
FROM ${CONTAINER_PROXY}node:lts-bullseye
```
